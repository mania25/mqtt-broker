var mosca = require('mosca')
var redis = require('redis')

var ascoltatore = {
  type: 'redis',
  redis: redis,
  return_buffers: true
}

var moscaSettings = {
  http: {
    port: 1884,
    bundle: true,
    static: './'
  },
  port: 1883,
  backend: ascoltatore,
  persistence: {
    factory: mosca.persistence.Redis
  }
}

var server = new mosca.Server(moscaSettings)

// Accepts the connection if the username and password are valid
var authenticate = function (client, username, password, callback) {
  var authorized = (username === 'bbff39d0d3066758ffe55666762b3c8b150295b848cb6c871b79f2fff36c79fb' &&
                password.toString() === '50acea3098359517297e08040dc6bfc371d044190be6527c1ac29e078cbe8313')

  if (authorized) {
    client.user = username
    callback(null, authorized)
  } else {
    callback(null, false)
  }
}

server.on('ready', function () {
  server.authenticate = authenticate
  console.log('Mosca server is up and running')
})

server.on('clientConnected', function (client) {
  console.log('client connected', client.id)
})

// fired when a message is received
server.on('published', function (packet, client) {
  console.log('Published', packet.topic, packet.payload)
})
